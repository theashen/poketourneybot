set -x
set -e

AWS_CLI_PROFILE=$1
STACK_NAME=$2
STACK_REGION=$3
CHALLONGE_USERNAME=$4
CHALLONGE_APIKEY=$5
TELEGRAM_BOT_TOKEN=$6
DEPLOYMENT_BUCKET=$7

sam build -u
sam package --output-template-file packaged.yaml --s3-bucket "$DEPLOYMENT_BUCKET" --profile "$AWS_CLI_PROFILE"
aws cloudformation deploy \
      --template-file packaged.yaml \
      --region "$STACK_REGION" \
      --capabilities CAPABILITY_IAM \
      --stack-name "$STACK_NAME" \
      --profile "$AWS_CLI_PROFILE"\
      --parameter-overrides ChallongeUserName="$CHALLONGE_USERNAME" \
                            ChallongeAPIKey="$CHALLONGE_APIKEY" \
                            TelegramToken="$TELEGRAM_BOT_TOKEN"

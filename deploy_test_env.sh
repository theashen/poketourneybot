set -x
set -e

CHALLONGE_APIKEY=$1
TELEGRAM_BOT_TOKEN=$2

sam build
sam package --output-template-file packaged.yaml --s3-bucket pogopvp-deployment-staging
aws cloudformation deploy \
      --template-file packaged.yaml \
      --region eu-west-2 \
      --capabilities CAPABILITY_IAM \
      --stack-name pogopvp-test \
      --parameter-overrides ChallongeUserName=poketourney_staging \
                            ChallongeAPIKey="$CHALLONGE_APIKEY" \
                            TelegramToken="$TELEGRAM_BOT_TOKEN"
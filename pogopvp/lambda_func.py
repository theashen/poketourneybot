import json

from telegram import Bot, Update
from telegram.ext import CommandHandler, CallbackQueryHandler, Dispatcher

from pogopvp.env import TELEGRAM_TOKEN
from pogopvp.storage import TourneyTable, SettingsTable
from pogopvp.tourney_manager import TourneyManager


def lambda_handler(event, context):

    print(event)

    bot = Bot(TELEGRAM_TOKEN)
    dispatcher = Dispatcher(bot, None, workers=0, use_context=True)

    tourney_table = TourneyTable()
    settings_table = SettingsTable()
    tourney_manager = TourneyManager(tourney_table, settings_table, bot)

    dispatcher.add_handler(
        CommandHandler(
            'newtourney',
            tourney_manager.create_tourney,
        )
    )
    dispatcher.add_handler(
        CommandHandler(
            'starttourney',
            tourney_manager.start_tourney,
        )
    )
    dispatcher.add_handler(
        CommandHandler(
            'result',
            tourney_manager.report_result,
        )
    )
    dispatcher.add_handler(
        CommandHandler(
            'remove',
            tourney_manager.remove_participant,
        )
    )
    dispatcher.add_handler(
        CommandHandler(
            'finish',
            tourney_manager.conclude_tourney,
        )
    )
    dispatcher.add_handler(
        CommandHandler(
            'language',
            tourney_manager.language,
        )
    )
    dispatcher.add_handler(CommandHandler('help', tourney_manager.help_message))
    dispatcher.add_handler(CallbackQueryHandler(tourney_manager.edit_registration))

    update = Update.de_json(json.loads(event['body']), bot)
    dispatcher.process_update(update)

    return {
        "statusCode": 200,
        "body": json.dumps({})
    }

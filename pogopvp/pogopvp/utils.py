from html import escape


def mention_markdown(user_id, name):
    if isinstance(user_id, int):
        return u'[{}](tg://user?id={})'.format(name, user_id)


def mention_html(user_id, name):
    if isinstance(user_id, int):
        return u'<a href="tg://user?id={}">{}</a>'.format(user_id, escape(name, True))


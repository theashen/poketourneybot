import time
from decimal import Decimal
from typing import Set

from .challonge import ChallongeTourney, TourneyTypes
from .utils import mention_html


class Settings:
    def __init__(self, chat_id, language):
        self.chat_id = str(chat_id)
        self.language = language

    def to_dict(self):
        return {
            'chat_id': self.chat_id,
            'settings': {
                'language': self.language,
            },
        }

    @classmethod
    def from_dict(cls, d):
        return cls(d['chat_id'], d['settings']['language'])


class User:
    def __init__(self, id, name, full_name):
        self.id = id
        self.name = name
        self.full_name = full_name

    def __str__(self):
        return mention_html(self.id, self.full_name)

    def to_dict(self):
        return {
            'id': self.id,
            'full_name': self.full_name,
            'name': self.name,
        }

    @classmethod
    def from_dict(cls, d):
        return cls(int(d['id']), d['name'], d['full_name'])

    def __hash__(self):
        return hash(self.id)

    def __eq__(self, other):
        return self.id == other.id


class TourneyStatus:
    CREATED = 'created'
    STARTED = 'started'
    FINISHED = 'finished'


class Tourney:
    def __init__(
            self,
            message_id: str,
            chat_id: str,
            chat_name: str,
            title: str,
            initiator: User,
            start_time: str,
            tourney_type: TourneyTypes,
            going: Set[User],
            maybe: Set[User],
            not_going: Set[User],
            chall_tourney: ChallongeTourney,
            round: int,
            status: str = TourneyStatus.CREATED,
            started_at: int = -1):
        self.id = self.get_id(message_id, chat_id)
        self.message_id = message_id
        self.chat_id = str(chat_id)
        self.chat_name = chat_name
        self.title = title
        self.initiator = initiator
        self.start_time = start_time
        self.tourney_type = tourney_type
        self.going = going
        self.maybe = maybe
        self.not_going = not_going
        self.chall_tourney = chall_tourney
        self.round = round
        self.status = status
        self.started_at = started_at

    @property
    def is_swiss(self):
        return self.tourney_type == TourneyTypes.sw

    @classmethod
    def get_id(cls, message_id, chat_id):
        return '-'.join([str(message_id), str(chat_id)])

    @classmethod
    def from_dict(cls, tourney_dict):
        initiator = User.from_dict(tourney_dict['initiator'])
        going = set([
            User.from_dict(user_info)
            for user_info in tourney_dict['going']
        ])
        maybe = set([
            User.from_dict(user_info)
            for user_info in tourney_dict['maybe']
        ])
        not_going = set([
            User.from_dict(user_info)
            for user_info in tourney_dict['not_going']
        ])

        tourney_type_val = tourney_dict.get('tourney_type')
        if tourney_type_val:
            tourney_type = TourneyTypes[tourney_type_val]
        else:
            tourney_type = TourneyTypes['sw']

        chall_tourney = ChallongeTourney(
            tourney_dict['challonge_id'],
            tourney_dict['challonge_name'],
            tourney_dict['challonge_url'],
            tourney_type,
        )

        return Tourney(
            tourney_dict['message_id'],
            tourney_dict['chat_id'],
            tourney_dict['chat_name'],
            tourney_dict['title'],
            initiator,
            tourney_dict['start_time'],
            tourney_type,
            going,
            maybe,
            not_going,
            chall_tourney,
            tourney_dict['round'],
            tourney_dict['status'],
            float(tourney_dict['started_at']),
        )

    def add_going(self, member: User):
        self.going.add(member)
        self.not_going -= {member}
        self.maybe -= {member}

    def add_maybe(self, member: User):
        self.maybe.add(member)
        self.going -= {member}
        self.not_going -= {member}

    def add_not_going(self, member: User):
        self.not_going.add(member)
        self.going -= {member}
        self.maybe -= {member}

    def start(self):
        print("Starting tourney")
        self.started_at = time.time()
        self.status = TourneyStatus.STARTED

    def to_markdown(self):
        going = '\n'.join([str(u) for u in self.going])
        going = f'\n{going}' if going else going

        maybe = '\n'.join([str(u) for u in self.maybe])
        maybe = f'\n{maybe}' if maybe else maybe

        not_going = '\n'.join([str(u) for u in self.not_going])
        not_going = f'\n{not_going}' if not_going else not_going

        markdown = (
            f'*Tourney* {self.message_id}: {self.title}\n'
            f'*Type*: {self.tourney_type.value}\n'
            f'*Starts at*: {self.start_time}\n'
            f'*Initiator*: {self.initiator}\n'
            f'*Going ({len(self.going)}):* {going}\n'
            f'*Not going:* {not_going}\n'
            f'*Maybe:* {maybe}'
        )
        return markdown

    def to_html(self, words_translations):
        words_translations = words_translations.split('|')
        (
            tourney,
            starts_at,
            type,
            initiator,
            going_word,
            not_going_word,
            maybe_word,
        ) = words_translations
        going = '\n'.join([str(u) for u in self.going])
        going = f'\n{going}' if going else going

        maybe = '\n'.join([str(u) for u in self.maybe])
        maybe = f'\n{maybe}' if maybe else maybe

        not_going = '\n'.join([str(u) for u in self.not_going])
        not_going = f'\n{not_going}' if not_going else not_going

        html = (
            f'<b>{tourney}</b> {self.message_id}: {self.title}\n'
            f'<b>{starts_at}</b>: {self.start_time}\n'
            f'<b>{type}</b>: {self.tourney_type.value}\n'           
            f'<b>{initiator}</b>: {self.initiator}\n'
            f'<b>{going_word} ({len(self.going)}):</b> {going}\n'
            f'<b>{not_going_word}:</b> {not_going}\n'
            f'<b>{maybe_word}:</b> {maybe}'
        )
        return html

    def to_dict(self):
        return {
            'id': self.id,
            'message_id': self.message_id,
            'chat_id': self.chat_id,
            'chat_name': self.chat_name,
            'title': self.title,
            'initiator': self.initiator.to_dict(),
            'start_time': self.start_time,
            'tourney_type': self.tourney_type.name,
            'going': [user.to_dict() for user in self.going],
            'maybe': [user.to_dict() for user in self.maybe],
            'not_going': [user.to_dict() for user in self.not_going],
            'challonge_id': self.chall_tourney.id,
            'challonge_name': self.chall_tourney.name,
            'challonge_url': self.chall_tourney.url,
            'round': self.round,
            'status': self.status,
            'started_at': Decimal(self.started_at),
        }

from botocore.exceptions import ClientError
from challonge import ChallongeException
from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode, Update
from telegram.ext import CallbackContext

from .challonge import ChallongeTourney, TourneyTypes, UserNotFound, MatchNotFound, RoundNeeded
from .messages import Translator, get_available_languages
from .models import User, Tourney, TourneyStatus, Settings


class ParseError(Exception):
    pass


class TourneyManager:
    def __init__(self, storage, settings_table, bot):
        self._storage = storage
        self._settings_table = settings_table
        self._bot = bot

    def registration_markup(self, button_words):
        yes, no, maybe = button_words.split('|')
        keyboard = [[
            InlineKeyboardButton(yes, callback_data='yes'),
            InlineKeyboardButton(no, callback_data='no'),
            InlineKeyboardButton(maybe, callback_data='maybe'),
        ]]
        return InlineKeyboardMarkup(keyboard)

    @staticmethod
    def _parse_create_args(args):
        if len(args) < 2:
            raise ValueError()

        start_time = args[0]
        if len(args[1]) == 2:
            title = ' '.join(args[2:])
            try:
                tourney_type = TourneyTypes[args[1]]
            except KeyError:
                tourney_type = TourneyTypes['sw']  # Swiss tourney by default
        else:
            title = ' '.join(args[1:])
            tourney_type = TourneyTypes['sw']  # Swiss tourney by default

        return start_time, tourney_type, title

    def create_tourney(self, update: Update, context: CallbackContext):
        chat = update.effective_chat
        user = update.effective_user
        language = self._get_settings(chat.id).language
        translations = Translator(language, 'newtourney')

        try:
            args = context.args
            start_time, tourney_type, title = self._parse_create_args(args)
        except (KeyError, ValueError):
            chat.send_message(translations.hint)
            return

        print("Effective id: %s" % update.effective_user.id)
        print("Effective full name: %s" % update.effective_user.full_name)
        print("Effective chat id: %s" % update.effective_chat.id)
        print("Effective chat name: %s" % update.effective_chat.title)

        initiator = User(
            user.id,
            user.name,
            user.full_name,
        )

        message = chat.send_message(translations.create_in_progress)
        message_id = message.message_id
        chat_id = message.chat.id
        chat_name = message.chat.title

        try:
            challonge_tourney = ChallongeTourney.create_tourney(title, tourney_type)
        except ChallongeException as e:
            chat.send_message(translations.internal_error)
            return

        tourney = Tourney(
            message_id, chat_id, chat_name, title, initiator, start_time,
            tourney_type, set(), set(), set(), challonge_tourney, 1
        )

        try:
            self._storage.write_tourney_with_lock(tourney)
        except ClientError:
            chat.send_message(translations.internal_error)

        self._bot.edit_message_text(
            tourney.to_html(translations.registration_words),
            chat_id=update.effective_chat.id,
            message_id=message_id,
            reply_markup=self.registration_markup(translations.button_words),
            parse_mode=ParseMode.HTML,
        )

    def start_tourney(self, update: Update, context: CallbackContext):
        chat = update.effective_chat
        language = self._get_settings(chat.id).language
        translations = Translator(language, 'starttourney')

        try:
            tourney_id, = context.args
        except ValueError:
            chat.send_message(translations.hint)
            return

        try:
            tourney_json = self._storage.get_tourney(Tourney.get_id(tourney_id, chat.id))
        except KeyError:
            chat.send_message(translations.tourney_not_found)
            return

        try:
            tourney = Tourney.from_dict(tourney_json)
        except ClientError:
            chat.send_message(translations.internal_error)
            return

        if tourney.status == TourneyStatus.FINISHED:
            chat.send_message(translations.already_finished)
            return

        if tourney.status != TourneyStatus.CREATED:
            chat.send_message(translations.already_started)
            return

        if len(tourney.going) < 2:
            chat.send_message(translations.gather_more_people)
            return

        chall_tourney = tourney.chall_tourney
        try:
            chall_tourney.bulk_add_participants(tourney.going)
            chall_tourney.start_tourney()
            tourney.start()
            self._storage.write_tourney_with_lock(tourney)
        except (ChallongeException, ClientError) as e:
            print(f"Error: {e}")
            chat.send_message(translations.failed_to_start_tourney)
        else:
            chat.send_message(translations.tourney_started.format(chall_tourney.url))

    def edit_registration(self, update: Update, context: CallbackContext):
        query = update.callback_query
        message_id = query.message.message_id
        chat_id = query.message.chat.id
        option = query.data
        language = self._get_settings(chat_id).language
        translations = Translator(language, 'newtourney')

        with self._storage.lock_client.acquire_lock(str(message_id)):
            tourney_resp = self._storage.get_tourney(Tourney.get_id(message_id, chat_id))
            print("Tourney_resp: %s" % tourney_resp)
            tourney = Tourney.from_dict(tourney_resp)
            print("Creating user")
            effective_user = update.effective_user
            user = User(
                effective_user.id,
                effective_user.name,
                effective_user.full_name,
            )
            print("Printing option: %s" % option)
            if option == 'yes':
                tourney.add_going(user)
            if option == 'no':
                tourney.add_not_going(user)
            if option == 'maybe':
                tourney.add_maybe(user)

            self._storage.write_tourney(tourney)

            query.edit_message_text(
                text=tourney.to_html(translations.registration_words),
                reply_markup=self.registration_markup(translations.button_words),
                parse_mode=ParseMode.HTML)

    @staticmethod
    def _parse_score(score):
        parsed_score = score.split('-')
        if len(parsed_score) == 1:
            parsed_score = parsed_score[0].split(':')
        try:
            pl1_points, pl2_points = map(int, parsed_score)
        except ValueError:
            raise ParseError()

        if pl1_points > pl2_points:
            return pl1_points, pl2_points, 1, False
        if pl2_points > pl1_points:
            return pl1_points, pl2_points, 2, False
        return pl1_points, pl2_points, 0, True

    def _parse_round(self, round_str):
        if round_str[0] == 'w':
            return int(round_str[1:])
        elif round_str[0] == 'l':
            return -int(round_str[1:])
        else:
            return int(round_str)

    def _parse_result_command(self, args, with_round):
        requested_round = None
        try:
            if with_round:
                tourney_id, pl1, score, pl2, round_str = args
                requested_round = self._parse_round(round_str)
            else:
                tourney_id, pl1, score, pl2 = args
            tourney_id = int(tourney_id)
        except ValueError:
            raise ParseError()

        pl1_points, pl2_points, winner_num, tie = self._parse_score(score)

        winner = pl1 if winner_num == 1 else pl2
        loser = pl2 if winner_num == 1 else pl1

        return (
            tourney_id,
            pl1_points,
            pl2_points,
            winner,
            loser,
            tie,
            requested_round,
        )

    def _process_short_result_command(self, args, chat, reporter, with_round=False):
        language = self._get_settings(chat.id).language
        translations = Translator(language, 'result')

        try:
            pl1_points, pl2_points, winner_num, tie = self._parse_score(args[0])
            if with_round:
                requested_round = self._parse_round(args[1])
            else:
                requested_round = None
        except ParseError:
            chat.send_message(translations.hint_short)
            return

        winner_points = pl1_points if winner_num == 1 else pl2_points
        loser_points = pl1_points if winner_num == 2 else pl2_points
        winner = reporter if winner_num == 1 else None
        loser = reporter if winner_num == 2 else None

        if tie:
            winner, looser = reporter, reporter
            winner_points, loser_points = pl1_points, pl2_points

        try:
            tourney_dict = self._storage.get_active_tourney(chat.id)
        except IndexError:
            chat.send_message(translations.no_active_tourney)
            return

        try:
            tourney = Tourney.from_dict(tourney_dict)
        except ClientError:
            chat.send_message(translations.internal_error)
            return

        if tourney.status == TourneyStatus.FINISHED:
            chat.send_message(translations.tourney_finished)
            return

        try:
            results = tourney.chall_tourney.report_result_short(
                winner,
                loser,
                winner_points,
                loser_points,
                tie,
                tourney.round,
                requested_round,
            )
        except UserNotFound:
            chat.send_message(translations.user_not_found)
            return
        except MatchNotFound:
            chat.send_message(translations.match_not_found)
            return
        except RoundNeeded:
            chat.send_message(translations.round_needed_short)
            return

        return (
            tourney,
            *results,
        )

    def _process_result_command(self, args, chat, with_round=False):
        language = self._get_settings(chat.id).language
        translations = Translator(language, 'result')

        (
            tourney_id,
            pl1_points,
            pl2_points,
            winner,
            loser,
            tie,
            requested_round,
        ) = self._parse_result_command(args, with_round)

        try:
            tourney_json = self._storage.get_tourney(Tourney.get_id(tourney_id, chat.id))
        except KeyError:
            chat.send_message(translations.tourney_not_found)
            return

        try:
            tourney = Tourney.from_dict(tourney_json)
        except ClientError:
            chat.send_message(translations.internal_error)
            return

        if tourney.status == TourneyStatus.FINISHED:
            chat.send_message(translations.tourney_finished)
            return

        chall_tourney = tourney.chall_tourney
        try:
            round_finished, last_finished, was_reopened = chall_tourney.report_result(
                (pl1_points, pl2_points),
                winner,
                loser,
                tourney.round,
                requested_round,
                tie,
            )
        except UserNotFound:
            chat.send_message(translations.user_not_found)
            return
        except MatchNotFound:
            chat.send_message(translations.match_not_found)
            return
        except RoundNeeded:
            chat.send_message(translations.round_needed)
            return

        print(f"round finished: {round_finished}")
        print(f"last finished: {last_finished}")
        print(f"was re-opened: {was_reopened}")
        return tourney, round_finished, last_finished, was_reopened, winner, loser

    def report_result(self, update: Update, context: CallbackContext):
        args = context.args
        reporter_nick = update.effective_user.name
        chat = update.effective_chat
        language = self._get_settings(chat.id).language
        translations = Translator(language, 'result')

        if len(args) == 1:
            result = self._process_short_result_command(args, chat, reporter_nick)
        elif len(args) == 2:
            result = self._process_short_result_command(args, chat, reporter_nick, with_round=True)
        elif len(args) == 4:
            result = self._process_result_command(args, chat)
        elif len(args) == 5:
            result = self._process_result_command(args, chat, with_round=True)
        else:
            chat.send_message(translations.hint_long)
            return

        if result is None:
            return
        else:
            tourney, round_finished, last_finished, was_reopened, winner, loser = result

        chall_tourney = tourney.chall_tourney

        print(f"round finished: {round_finished}")
        print(f"last finished: {last_finished}")
        print(f"was re-opened: {was_reopened}")
        if was_reopened:
            chat.send_message(translations.resubmitted.format(winner, loser))
            return
        else:
            chat.send_message(translations.submitted.format(winner, loser))

        if round_finished and not last_finished and tourney.is_swiss:
            chat.send_message(
                translations.round_finished.format(tourney.round, chall_tourney.url)
            )
            tourney.round += 1
            self._storage.write_tourney_with_lock(tourney)
            return

        if round_finished and last_finished:
            chat.send_message(translations.all_rounds_finished)

    def remove_participant(self, update: Update, context: CallbackContext):
        args = context.args
        chat_id = update.effective_chat.id
        chat = update.effective_chat
        language = self._get_settings(chat.id).language
        translations = Translator(language, 'remove')

        if len(args) == 1:
            participant, = args
            try:
                tourney_json = self._storage.get_active_tourney(chat_id)
            except IndexError:
                chat.send_message(translations.tourney_not_found)
                return
        elif len(args) == 2:
            tourney_id, participant = args
            try:
                tourney_json = self._storage.get_tourney(Tourney.get_id(tourney_id, chat_id))
            except KeyError:
                chat.send_message(translations.tourney_not_found)
                return
        else:
            chat.send_message(translations.hint)
            return

        # Workaround for people without nicknames
        participant = participant.replace('+', ' ')

        try:
            tourney = Tourney.from_dict(tourney_json)
        except ClientError:
            chat.send_message(translations.internal_error)
            return

        status = chat.get_member(update.effective_user.id).status
        reporter = update.effective_user
        print('reporter status: %s' % status)
        if not (status in ['administrator', 'creator'] or tourney.initiator.full_name == reporter.full_name):
            chat.send_message(translations.not_authorized)
            return

        if tourney.status == TourneyStatus.FINISHED:
            chat.send_message(translations.already_finished)
            return

        chall_tourney = tourney.chall_tourney
        try:
            round_finished, last_finished = chall_tourney.remove_participant(participant, tourney.round)
        except UserNotFound:
            chat.send_message(translations.user_not_found)
            return

        chat.send_message(translations.participant_removed.format(participant))

        if round_finished and not last_finished and tourney.is_swiss:
            chat.send_message(
                translations.round_finished.format(tourney.round, chall_tourney.url)
            )
            tourney.round += 1
            self._storage.write_tourney_with_lock(tourney)
            return

        if round_finished and last_finished:
            chat.send_message(translations.all_rounds_finished)

    def conclude_tourney(self, update: Update, context: CallbackContext):
        args = context.args
        chat_id = update.effective_chat.id
        chat = update.effective_chat
        language = self._get_settings(chat.id).language
        translations = Translator(language, 'finish')

        if len(args) == 0:
            try:
                tourney_json = self._storage.get_active_tourney(chat_id)
            except IndexError:
                chat.send_message(translations.tourney_not_found)
                return
        elif len(args) == 1:
            tourney_id, = args
            try:
                tourney_json = self._storage.get_tourney(Tourney.get_id(tourney_id, chat_id))
            except KeyError:
                chat.send_message(translations.tourney_not_found)
                return
        else:
            chat.send_message(translations.hint)
            return

        try:
            tourney = Tourney.from_dict(tourney_json)
        except ClientError:
            chat.send_message(translations.internal_error)
            return

        status = chat.get_member(update.effective_user.id).status
        reporter = update.effective_user
        print('reporter status: %s' % status)
        if not (status in ['administrator', 'creator'] or tourney.initiator.full_name == reporter.full_name):
            chat.send_message(translations.not_authorized)
            return

        if tourney.status == TourneyStatus.FINISHED:
            chat.send_message(translations.already_finished)
            return

        chall_tourney = tourney.chall_tourney
        results = chall_tourney.finalize()
        participants = results['participants']
        res_dict = {}
        for participant in participants:
            p = participant['participant']
            res_dict[p['name']] = p['final_rank']
        sorted_results = sorted(list(res_dict.items()), key=lambda tup: tup[1])
        message = ''
        if len(sorted_results) > 2:
            first, second, third = [p[0] for p in sorted_results[:3]]
            message = translations.congratulation.format(first, second, third)
        if len(sorted_results) == 2:
            first, second = [p[0] for p in sorted_results[:2]]
            message = translations.congratulation_for_two.format(first, second)
        chat.send_message(message)
        tourney.status = TourneyStatus.FINISHED
        self._storage.write_tourney_with_lock(tourney)

    def _get_settings(self, chat_id):
        try:
            settings_dict = self._settings_table.get_settings(chat_id)
            current_settings = Settings.from_dict(settings_dict)
        except KeyError:
            default_settings = Settings(chat_id, 'en')
            self._settings_table.write_settings(default_settings)
            current_settings = default_settings

        return current_settings

    def language(self, update: Update, context: CallbackContext):
        args = context.args
        chat = update.effective_chat
        language = self._get_settings(chat.id).language
        translations = Translator(language, 'language')
        languages = get_available_languages()

        if len(args) == 0:
            chat.send_message(translations.available_languages.format(', '.join(languages), language))
            return

        if len(args) == 1:
            new_language, = args
            if new_language not in languages:
                chat.send_message(translations.not_supported)
                return
            new_settings = Settings(chat.id, new_language)
            self._settings_table.write_settings(new_settings)
            translations = Translator(new_language, 'language')
            chat.send_message(translations.language_updated.format(new_language))

    def help_message(self, update: Update, context: CallbackContext):
        chat = update.effective_chat
        language = self._get_settings(chat.id).language
        translations = Translator(language, 'help')
        chat.send_message(text=translations.help, parse_mode=ParseMode.MARKDOWN)


class Translator:
    def __init__(self, language, command):
        self.language = language
        self.command = command

    def __getattr__(self, name):
        if name in translations[self.language]["common"]:
            return translations[self.language]["common"][name]
        return translations[self.language][self.command][name]


def get_available_languages():
    return list(translations.keys())


translations = dict(
    en=dict(
        newtourney=dict(
            hint=
            """Please, provide start time, type and description in next format:
/newtourney hh:mm tourney_type Tourney Description
Example:
/newtourney 22:00 sw Great League Random
Available tourney types:
  sw - swiss
  rr - round robin
  se - single elimination
  de - double elimination
""",
            create_in_progress="Creating...",
            registration_words="Tourney|Starts at|Type|Initiator|Going|Not going|Maybe",
            button_words="Yes|No|Maybe",
        ),

        starttourney=dict(
            hint=
            """Please, provide id of tourney (find it in registration message)
Example:
/starttourney 241
""",
            tourney_not_found=
            """Tourney not found. Please, provide correct id of tourney (find it in registration message)
Example:
/starttourney 241
""",
            already_finished=
            """This tourney is already finished. Maybe id is wrong?
Example:
/starttourney 241
""",
            already_started="This tourney is already started",
            gather_more_people="Please, gather more people and try again",
            failed_to_start_tourney="Failed to start tourney",
            tourney_started=
            """Your tourney has started. Good luck!
Check details here: {}
""",
        ),

        result=dict(
            hint_short=
            """Please, provide score in correct format. For example:
/result 2:1
""",

            no_active_tourney="Not able to get active tourney. Maybe you forgot to start one?",
            hint_long=
            """Please, provide result in correct format.
Full version:
/result 241 @neo 2:1 @morpheus
(command, tourney id, nick1, score, nick2 separated by spaces)
Short version:
/result 2:1""",
            tourney_not_found="Tourney not found. Maybe id is wrong?",
            user_not_found="You are not among participants",
            match_not_found="Your match is not found. Maybe you have a bye?",
            tourney_finished="This tourney is already finished. You can report results only to ongoing tourneys 😉",
            submitted="Result of match between {} and {} was submitted",
            resubmitted="Result of match between {} and {} was resubmitted",
            round_needed=
            """Found few matches for this pair. Please, specify round.
Rounds in winners bracket should be prepended with 'w' (e.g. w1, w2), 'l' - for losers bracket (e.g. l1, l2).
For example:
/result 10251 @neo 2:1 @morpheus w1
/result 10251 @trinity 2:1 @morpheus l1
            """,
            round_needed_short=
            """Found few matches in several rounds. Please, specify round delimited by space like this:
/result 2:1 1
            """,
            round_finished=
            """Round {} finished.
Check your next opponent here: {}""",
            all_rounds_finished="All rounds are finished. You can conclude the tournament. But make sure that all results are correct first 😉" ,
        ),

        remove=dict(
            hint=
            """Please, use next formats for this command:
Short:
/remove @neo
Full:
/remove 241 @neo
""",
            tourney_not_found="There is no active tourney in this chat",
            already_finished="Not able to remove participant: active tourney is finished",
            user_not_found="Not able to find this user in active tournament",
            not_authorized="Only admins or tourney initiator are allowed to remove participants",
            participant_removed=(
                "Participant {} is removed from active tourney. "
                "They got a tech lose in ongoing matches and will not take part in future matches"
            ),
            round_finished=
            """Round {} finished.
Check your next opponent here: {}""",
            all_rounds_finished="All rounds are finished. You can conclude the tournament. But make sure that all results are correct first 😉" ,
        ),

        finish=dict(
            hint=
            """Please, provide id of tourney (find it in registration message) or use a short variant.
Example:
/finish 241""",
            tourney_not_found=
            """Tourney not found. Please, provide correct id of tourney (find it in registration message)
Example:
/finish 241""",
            not_authorized="""Only admins or tourney initiator are allowed to close tourney""",
            already_finished=
            """This tourney is already finished. Please, provide id of tourney if you want to finish older (not active) tourney
Example:
/finish 241""",
            congratulation=
            """Supreme Champion: {}
Worthy Adversary: {}
Maybe Next Time: {}

Congratulations to all participants! Good luck in next tournaments!""",
            congratulation_for_two=
            """Supreme Champion: {}
Worthy Adversary: {}

Congratulations to all participants! Good luck in next tournaments!""",
        ),

        language=dict(
            available_languages=
            """Currently I can speak {}. My language in this chat is: {}. You can choose language like this:
/language en""",
            not_supported="Sorry, this language is not supported yet",
            language_updated="Updated your chat language to {}",
        ),

        help=dict(
            help=
            """*Description*
            
This bot helps to organize tournaments:
  - swiss system,
  - round robin,
  - single elimination,
  - double elimination.

*Commands*

To initiate a new tournament send /newtourney@PokeTourneyBot command in a group chat.
If there are no other bots that react to /newtourney command, then no need to append @PokeTourneyBot to it. For example:

_/newtourney@PokeTourneyBot 22:00 sw Great League, types: Ice, Normal, Fairy, Grass_

22:00 - tournament start time.
sw - tournament type (sw, rr, se, de).
Everything you type after the tournament type will be a title.
This command creates a message, where people can register for the tournament.
You can omit tournament type: swiss will be created by default.

_/starttourney 10251_

Starts tourney with id 10251. You will find id in the registration message after you create a tourney.
Bot then publishes a message with url to challonge.com, where actual tourney is created.

_/result 10251 @neo 2:1 @morpheus_

Registers result of match between players with telegram nicknames @neo and @morpheus.
Score is reported in format 2:1. Tie is reported as 0:0.
If you want to change results of particular match, just edit the original message or post new one with correct results.
With this version of command you can edit matches even from previous rounds, which is not possible with short command (see "Short commands" below).
Pairs of current round may change after editing match from previous round.
In some cases in double elimination tourney you may be required to specify round:

_/result 10251 @neo 2:1 @morpheus w1_
_/result 10251 @trinity 2:1 @morpheus l1_

Rounds in winners bracket should be prepended with 'w' (e.g. w1, w2), 'l' - for losers bracket (e.g. l1, l2).

After all rounds are finished, you should finalize tourney. Make sure that all results at challonge.com are correct.
You are able to edit them (with bot commands) only before tourney is finalized.

_/finish 10251_

Concludes tournament. Bot will respond with results of your tourney. Only chat admins or tourney initiator is able to finish tourney.

_/remove 10251 @neo_

Removes participant from tournament. They got a tech lose in ongoing matches and will not take part in future matches.
Only chat admins or tourney initiator is able to remove participant. Be careful! This command is not reversible. You will not be able to restore removed participant.

*Short commands*

To make commands easier (especially results reporting), you can use short variants for _/result_, _/finish_, _/remove_.
These commands will be applied for current "active tournament". "Active tournament" - one which is started
most recently (last tourney started with command /starttourney).

_/result 2:1_

It will submit reporter's win with score 2:1.
In some cases (in round robin tourney) you would have to specify round like this:

_/result 2:1 2_

Round comes after score, separated by space.

_/finish_

Finishes current active tournament.

_/remove @neo_

Removes participant with nick @neo from tournament.

Good luck in tournaments!

If you have suggestions, additional questions or something doesn't work right, ask in telegram group: https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ
""",
        ),

        common=dict(
            internal_error="Oops, something went wrong. If it happens again, write to https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ",
        )
    ),

    ua=dict(
        newtourney=dict(
            hint=
            """Будь ласка, задайте час початку, тип та назву турніру в наступному форматі:
/newtourney hh:mm tourney_type Назва турніру
Наприклад:
/newtourney 22:00 sw Грейт ліга рендом
Доступні типи турнірів:
  sw - швейцарська система
  rr - всі з усіма
  se - турнір на вибування
  de - турнір на вибування з сіткою переможених
""",
            create_in_progress="Створюю...",
            registration_words="Турнір|Початок|Тип|Ініціатор|Візьмуть участь|Не виходить|Можливо",
            button_words="Візьму участь|Не виходить|Можливо",
        ),
        starttourney=dict(
            hint=
            """Будь ласка, вкажіть айді турніру (можна взяти з повідомлення про створення турніру)
Наприклад:
/starttourney 241
""",
            tourney_not_found=
            """Не можу знайти турнір. Будь ласка, вкажіть правильний айді турніру (можна взяти з повідомлення про створення турніру). Наприклад:
/starttourney 241
""",
            already_finished="""Цей турнір вже закінчився. Можливо, ввели неправильний айді?""",
            already_started="Цей турнір вже стартували",
            gather_more_people="Будь ласка, зберіть більше учасників і спробуйте ще раз 😉",
            failed_to_start_tourney="Не вдалося створити турнір 😞",
            tourney_started=
            """Турнір розпочався. Удачі!
Деталі за посиланням: {}
""",
        ),

        result=dict(
            hint_short=
            """Будь ласка, повідомте рахунок у правильному форматі. Наприклад:
/result 2:1
Першими йдуть очки того, хто пише результат.
""",

            no_active_tourney="Не можу знайти активний турнір. Можливо, Ви його не стартували?",
            hint_long=
            """Будь ласка, повідомте рахунок у правильному форматі.
Скорочена версія. У скороченій першими йдуть очки того, хто пише результат:
/result 2:1
Розширена версія команди:
/result 241 @neo 2:1 @morpheus
""",
            tourney_not_found="Не можу знайти турнір. Можливо, вводите неправильний айді?",
            user_not_found="Ви не учасник турніру...",
            match_not_found="Не можу знайти матч. Можливо, у Вас бай?",
            tourney_finished="Цей турнір вже закінчився. Ви можете повідомляти рахунок тільки у відкритому турнірі 😉",
            submitted="Результат матчу між {} та {} прийнято",
            resubmitted="Результат матчу між {} та {} було змінено",
            round_needed=
            """Для цієї пари знайдено декілька матчів. Будь ласка, вкажіть раунд.
Раунд в сітці переможців потрібно вказувати з префіксом 'w' (наприклад w1, w2), 'l' - для сітки переможених (наприклад l1, l2):
/result 10251 @neo 2:1 @morpheus w1
/result 10251 @trinity 2:1 @morpheus l1
""",
            round_needed_short=
            """Знайдено матчі в декількох раундах. Вкажіть, будь ласка, потрібний раунд через пробіл:
/result 2:1 1
            """,
            round_finished=
            """Раунд {} завершено.
Опонента в наступному раунді можна знайти тут: {}""",
            all_rounds_finished="Всі раунди зіграні. Переконайтесь, що введені результати правильні, і закінчуйте турнір",
        ),

        remove=dict(
            hint=
            """Будь ласка, вводьте цю команду в наступному форматі:
Розширена версія команди:
/remove 241 @neo       
Скорочена версія:
/remove @neo
""",
            tourney_not_found="В цьому чаті на даний момент немає активного турніру",
            already_finished="Не можу видалити учасника: турнір вже завершено",
            user_not_found="Не можу знайти цього учасника в активному турнірі",
            not_authorized="Тільки ініціатор турніру або адміністратори чату можуть видаляти учасників турніру",
            participant_removed=(
                "Учасника/цю {} видалено з турніру. "
                "Видалені учасники отримують технічну поразку в поточному матчі і не будуть брати участь в наступних матчах."
            ),
            round_finished=
            """Раунд {} завершено.
Опонента в наступному раунді можна знайти тут: {}""",
            all_rounds_finished="Всі раунди зіграні. Переконайтесь, що введені результати правильні, і закінчуйте турнір",
        ),

        finish=dict(
            hint=
            """Будь ласка, вкажіть айді турніру (можна взяти з повідомлення про створення турніру). Наприклад:
/finish 241""",
            tourney_not_found=
            """Не можу знайти турнір. Будь ласка, вкажіть правильний айді турніру (можна взяти з повідомлення про створення турніру). Наприклад:
/finish 241""",
            already_finished="""Цей турнір вже закінчився. Можливо, ввели неправильний айді?""",
            not_authorized="Тільки ініціатор турніру або адміністратори чату завершити турнір",
            congratulation=
            """Чемпіон: {}
Гідний суперник: {}
Можливо наступного разу: {}

Вітання всім учасникам! Удачі в наступних турнірах!""",
            congratulation_for_two=
            """Чемпіон: {}
Гідний суперник: {}

Вітання всім учасникам! Удачі в наступних турнірах!""",
        ),

        language=dict(
            available_languages=
            """Я можу розмовляти наступними мовами: {}. Зараз вибрано {}. Ви можете вибрати мову для чату ось так:
/language en""",
            not_supported="Вибачте, ця мова поки що не підтримується",
            language_updated="Мову змінено на {}",
        ),

        help=dict(
            help="""*Що це за бот?*

Цей бот допомагає організовувати турніри:
  - швейцарська система,
  - всі з усіма,
  - турнір на вибуття,
  - турнір на вибуття з сіткою переможених.

*Команди*

Для того, щоб розпочати реєстрацію на новий турнір, введіть команду /newtourney@PokeTourneyBot у вашому чаті.
Якщо в чаті немає інших ботів, які реагують на команду /newtourney, можете не додавать до неї @PokeTourneyBot.

Наприклад:

_/newtourney@PokeTourneyBot 22:00 sw Great League, types: Ice, Normal, Fairy, Grass_

22:00 - час початку турніру.
sw - тип турніру (sw, rr, se, de).
Все, що ви введете після типу турніру, буде його назвою.
Ця команда створює повідомлення про реєстрацію, де всі бажаючі грати можуть відмітитись.
Ви можете не вказувати тип турніру: буде створено турнір з швейцарською системо за умовчуванням.

_/starttourney 10251_

Стартує турнір з айді 10251. Всі, хто натиснув Yes в повідомленні про реєстрацію, стають учасниками.
Турнір створюється на сайті challonge.com, після чого бот публікує посилання на турнір в чаті.

_/result 10251 @neo 2:1 @morpheus_

Реєструє результат матчу між гравцями з ніками @neo та @morpheus.
Рахунок потрібно вводити в наступному форматі: 2:1. Нічия вводиться в такому ж форматі: 0:0
Якщо потрібно змінити неправильно введені результати матчу, достатньо відредагувати повідомлення.
В повній версії команди ви можете редагувати навіть матчі з попередніх раундів. Що неможливо в короткій версії (див. "Короткі команди")
Після редагування матчів попередніх раундів пари поточного раунду можуть змінитися.
В деяких випадках в турнірі на вибуття з сіткою переможених вам потрібно буде вказати раунд:

_/result 10251 @neo 2:1 @morpheus_ w1
_/result 10251 @trinity 2:1 @morpheus_ l1

Раунд в сітці переможців вказується з префіксом 'w' (наприклад w1, w2), 'l' - для сітки переможених (наприклад l1, l2).

Після того як всі раунди зігарно, потрібно завершити турнір. Переконайтеся, що результати матчів на challonge.com правильні: після завершення турніру редагувати їх вже не буде можливості.

_/finish 10251_

Закриває турнір, бот повідомить його результати. Завершити турнір може тільки його ініціатор або адміністратори чату.

_/remove 10251 @neo_

Таким чином можна видалити учасника/цю. Після видалення учасник/ця отримає технічну поразку в поточному турнірі і не буде брати участь в наступних матчах.
Видалити з турніру може тільки його ініціатор або адміністратори чату. 

*Короткі команди*

Для того, щоб спростити команди (особливо введення результатів матчу), можете використовувати короткі варіанти для
_/result_, _/finish_, _/remove_. Ці команди будуть застосовані до поточного "активного турнуру". "Активний турнір" -
останній турнір, ропочатий командою _/starttourney_.

_/result 2:1_

Реєструє виграш того, хто написав команду з рахунком 2:1
У деяких випадках (турнір "всі з усіма") потрібно вказувати раунд:

_/result 2:1 2_

Раунд вказується через пробіл зразу за рахунком.

_/finish_

Завершує поточний "активний турнір".

_/remove_ @neo

Видаляє учасника з ніком @neo з "активного турніру".

Удачі в турнірах!
Якщо у вас виникли пропозиції, додаткові запитання або щось працює не так як потрібно, запитуйте в телеграм каналі: https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ
""",
        ),

        common=dict(
            internal_error="Упс, щось пішло не так... Якщо щось подібне повториться, пишіть сюди: https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ",
        )
    ),
    ru=dict(
        newtourney=dict(
            hint=
            """Пожалуйста, задайте время начала, тип и название турнира в следующем формате:
/newtourney hh:mm tourney_type Название турнира
Например:
/newtourney 22:00 sw Грэйт лига рандом
Типы турниров:
  sw - швейцарская система
  rr - все со всеми
  se - турнир на выбывание
  de - турнир на выбывание с сеткой побежденных
""",
            create_in_progress="Создаю...",
            registration_words="Турнир|Начало|Тип|Инициатор|Примут участие|Не получается|Возможно",
            button_words="Приму участие|Не получается|Возможно",
        ),
        starttourney=dict(
            hint=
            """Пожалуйста, укажите айди турнира (можно найти в сообщении о создании турнира)
Например:
/starttourney 241
""",
            tourney_not_found=
            """Не могу найти турнир. Пожалуйста, укажите правильный айди турнира (можно найти в сообщении о создании турнира). Например:
/starttourney 241
""",
            already_finished="""Этот турнир уже закончился. Возможно, ввели неправильный айди?""",
            already_started="Этот турнир уже стартовали",
            gather_more_people="Пожалуйста, соберите больше участников и попробуйте еще раз 😉",
            failed_to_start_tourney="Не удалось создать турнир 😞",
            tourney_started=
            """Турнир начался. Удачи!
Детали по ссылке: {}
""",
        ),

        result=dict(
            hint_short=
            """Пожалуйста, сообщите счет в правильном формате. Например:
/result 2:1
Первыми идут очки того, кто пишет результат.
""",

            no_active_tourney="Не могу найти активный турнир. Может, Вы его не стартовали?",
            hint_long=
            """Пожалуйста, сообщите счет в правильном формате.            
Сокращенная версия. В сокращенной версии первыми идут очки того, кто пишет результат:
/result 2:1
Расширенная версия команды:
/result 241 @neo 2:1 @morpheus
""",
            tourney_not_found="Не могу найти турнир. Может, вводите неправильный айди?",
            user_not_found="Вы не участник турнира...",
            match_not_found="Не могу найти матч. Возможно, у Вас бай?",
            tourney_finished="Этот турнир уже закончился. Вы можете сообщать счет только в открытом турнире 😉",
            submitted="Результат матча между {} и {} принят",
            resubmitted="Результат матча между {} и {} был изменен",
            round_needed=
            """Для этой пары найдено несколько матчей. Пожалуйста, укажите нужный раунд.
Раунд в сетке победителей нужно указывать с префиксом 'w' (наприклад w1, w2), 'l' - для сетки побежденных (например l1, l2):
/result 10251 @neo 2:1 @morpheus w1
/result 10251 @trinity 2:1 @morpheus l1
""",
            round_needed_short=
            """Найдено матчи в нескольких раундах. Укажите, пожалуйста, нужный раунд через пробел:
/result 2:1 1
            """,
            round_finished=
            """Раунд {} окончен.
Оппонента в следующем раунде можно найти здесь: {}""",
            all_rounds_finished="Все раунды сыграны. Убедитесь, что все результаты верны, и можете заканчивать турнир",
        ),

        remove=dict(
            hint=
            """Пожалуйста, введите команду в следующем формате:
Расширенная версия команды:
/remove 241 @neo       
Сокращенная версия:
/remove @neo
""",
            tourney_not_found="В этом чате на данный момент нет активного турнира",
            already_finished="Не могу удалить участника: турнир уже окончился",
            user_not_found="Не могу найти этого участника в активном турнире",
            not_authorized="Только инициатор турнира или администраторы чата могут удалять участников турнира",
            participant_removed=(
                "Участник удален с турнира. "
                "Удаленные участники получают техническое поражение в текущем матче и не будут участвовать в следующих матчах"
            ),
            round_finished=
            """Раунд {} окончен.
Оппонента в следующем раунде можно найти здесь: {}""",
            all_rounds_finished="Все раунды сыграны. Убедитесь, что все результаты верны, и можете заканчивать турнир",
        ),

        finish=dict(
            hint=
            """Пожалуйста, укажите айди турнира (можно найти в сообщении о создании турнира). Например:
/finish 241""",
            tourney_not_found=
            """Не могу найти турнир. Пожалуйста, укажите правильный айди турнира (можно найти в сообщении о создании турнира). Например:
/finish 241""",
            already_finished="""Этот турнир уже закончился. Возможно, ввели неправильный айди?""",
            not_authorized="Только инициатор турнира или администраторы чата могут завершить турнир",
            congratulation=
            """Чемпион: {}
Достойный соперник: {}
Возможно в следующий раз: {}

Поздравления всем участникам! Удачи в следующих турнирах!""",
            congratulation_for_two=
            """Чемпион: {}
Достойный соперник: {}

Поздравления всем участникам! Удачи в следующих турнирах!""",
        ),

        language=dict(
            available_languages=
            """Я могу разговаривать на следующих языках: {}. Вы можете выбрать язык вот так:
/language en""",
            not_supported="Извините, этот язык пока что не поддерживается",
            language_updated="Язык изменен на {}",
        ),

        help=dict(
            help="""*Что это за бот?*

Этот бот помогает организовывать турниры:
  - швейцарская система,
  - все со всеми,
  - турнир на выбывание,
  - турнир на выбывание с сеткой побежденных.

*Команды*

Для того, чтобы начать регистрацию на новый турнир, введите команду /newtourney@PokeTourneyBot в вашем чате.
Если в чате нет других ботов, которые реагируют на команду /newtourney, можете не добавлять к ней @PokeTourneyBot.

Например:

_/newtourney@PokeTourneyBot 22:00 sw Great League, types: Ice, Normal, Fairy, Grass_

22:00 - время начала турнира.
sw - тип турнира.
Все, что вы введете после типа турнира, будет его названием.
Эта команда создает сообщение о регистрации, где все желающие могут отметиться.
Вы можете не указывать тип турнира: будет создан швейцарский по умолчанию.

_/starttourney 10251_

Стартует турнир с айди 10251. Все, кто нажал Yes в сообщении о регистрации, становятся участниками.
Турнир создается на сайте challonge.com, после чего бот публикует ссылку на турнир в чате.

_/result 10251 @neo 2:1 @morpheus_

Регистрирует результат матча между игроками с никами @neo и @morpheus
Счет нужно вводить в следующем формате: 2:1. Ничья вводится в таком же формате: 0:0.
Если нужно изменить неправильно введенные результаты матча, достаточно отредактировать сообщение или написать заново.
В полной версии команды вы можете редактировать даже матчи с предыдущих раундов. Это невозможно в сокращенной версии (см. "Сокращенные команды")
После редактирования матчей предыдущих раундов пары текущего раунда могут измениться.
В некоторых случаях в турнире на выбывание с сеткой побежденных вам нужно будет указать раунд:

_/result 10251 @neo 2:1 @morpheus w1_
_/result 10251 @trinity 2:1 @morpheus l1_

У раунда для сетки победителей должен быть префикс 'w' (например w1, w2), 'l' - для сетки побежденных (например l1, l2).

После того как все раунды сыграны, нужно завершить турнир. Убедитесь, что результаты матчей на challonge.com правильные: после завершения турнира изменить их уже не будет возможности.

_/finish 10251_

Завершает турнир, бот сообщит о результатах. Завершить турнир может только его инициатор или администраторы чата.

_/remove 10251 @neo_

Таким образом можно удалить участника турнира. После удаления участник получает техническое поражение в текущем матче и не будет участвовать в следующих матчах.
Удалить с турнира может только его инициатор или администраторы чата.

*Сокращенные команды*

Для того, чтобы упростить команды (особенно введение результатов матча), можете использовать сокращенные варианты для
_/result_, _/finish_, _/remove_. Эти команды будут применены к текущему "активному турниру".
"Активный турнир" - последний турнир, начатый командой _/starttourney_.

_/result 2:1_

Регистрирует победу того, кто написал команду, со счетом 2:1.
В некоторых случаях (в турнире "все со всеми") вы должны указать раунд следующим образом:

_/result 2:1 2_

Раунд идет после счета, через пробел.

_/finish_

Завершает текущий "активный турнир".

_/remove_ @neo

Удаляет участника с ником @neo с "активного турнира".

Удачи в турнирах!
Если у вас возникли предложения, дополнительные вопросы или чтото работает не так, обращайтесь в телеграм группу: https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ
""",
        ),

        common=dict(
            internal_error="Упс, чтото пошло не так... Если подобное повторится, пишите в телеграм группу: https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ",
        )
    ),
    
    es=dict(
        newtourney=dict(
            hint=
            """Por favor, indica hora de inicio y descripción en el siguiente formato:
/newtourney hh:mm tourney_type Tourney descripción
Ejemplo:
/newtourney 22:00 sw Liga Ultra, aleatorio
Tipos de torneos disponibles:
  sw - suizo
  rr - round robin
  se - eliminación simple
  de - doble eliminación
""",
            create_in_progress="Creando...",
            registration_words="Torneo|Comienza|Tipo|Iniciador|Participar|No participará|Tal vez",
            button_words="Voy a participar|No participaré|Tal vez",
        ),

        starttourney=dict(
            hint=
            """Por favor, indica el id del torneo (puedes encontrarlo en el mensaje de registro)
Ejemplo:
/starttourney 241
""",
            tourney_not_found=
            """Torneo no encontrado. Por favor, indica el id correcto del torneo
(puedes encontrarlo en el mensaje de registro)
Ejemplo:
/starttourney 241
""",
            already_finished=
            """Este torneo ya ha finalizado. ¿id incorrecto?
Ejemplo:
/starttourney 241
""",
            already_started="Este torneo ya ha comenzado",
            gather_more_people="Por favor, reúne a más gente y prueba de nuevo",
            failed_to_start_tourney="Fallo al iniciar el torneo",
            tourney_started=
            """El torneo ha comenzado. ¡Buena suerte!
Detalles del torneo: {}
""",
        ),

        result=dict(
            hint_short=
            """Por favor, indica el resultado en el formato correcto. Por ejemplo:
/result 2:1
""",

            no_active_tourney="No se puede obtener el torneo activo. ¿Quizás no se inició uno?",
            hint_long=
            """Por favor, indica el resultado en el formato correcto.
Versión larga:
/result 241 @neo 2:1 @morpheus
(comando, id_torneo, nick1, puntuación, nick2 separados por espacios)
Versión corta:
/result 2:1""",
            tourney_not_found="No se encontró el torneo. ¿id incorrecto?",
            user_not_found="No eres participante del torneo",
            match_not_found="No encuentro tu combate. ¿Quizás tienes un bye?",
            tourney_finished="Este torneo ya ha finalizado, solo puedes reportar resultados en un torneo activo 😉",
            submitted="Resultado del combate entre {} y {} enviado",
            resubmitted="Resultado del combate entre {} y {} reenviado",
            round_needed=
            """Hay pocos partidos para este par. Por favor, especifique la ronda.
Las rondas en el grupo de ganadores deben anteponerse con 'w' (por ejemplo, w1, w2), 'l' - para el grupo de perdedores (por ejemplo, l1, l2):
/result 10251 @neo 2:1 @morpheus w1
/result 10251 @trinity 2:1 @morpheus l1
""",
            round_needed_short=
            """Encontrado pocos partidos en varias rondas. Por favor, especifique la ronda delimitada por un espacio:
/result 2:1 1
            """,
            round_finished=
            """Ronda {} terminada.
Comprueba aquí tu siguiente oponente: {}""",
            all_rounds_finished="Todas las rondas han terminado. Puedes finalizar el torneo, pero primero verifica que todos los resultados son correctos 😉",
        ),

        remove=dict(
            hint=
            """Por favor utiliza uno de los siguientes formatos para este comando:
Corto:
/remove @neo
Largo:
/remove 241 @neo
""",
            tourney_not_found="No hay torneo activo en este chat",
            already_finished="No es posible eliminar un usuario, el torneo activo ha finalizado",
            user_not_found="No se ha podido encontrar este usuario en un torneo activo",
            not_authorized="Solo los administradores u organizadores de torneos pueden eliminar usuarios",
            participant_removed=(
                "Participante {} eliminado del torneo. "
                "Obtendrá una derrota técnica en los combates pendientes y no participará en más combates"
            ),
            round_finished=
            """Ronda {} terminada.
Comprueba aquí tu siguiente oponente: {}""",
            all_rounds_finished="Todas las rondas han terminado. Puedes finalizar el torneo, pero primero verifica que todos los resultados son correctos 😉",
        ),

        finish=dict(
            hint=
            """Por favor, indica el id del torneo (puedes encontrarlo en el mensaje de registro)
Ejemplo:
/finish 241""",
            tourney_not_found=
            """Torneo no encontrado. Por favor, indica el id del torneo
(puedes encontrarlo en el mensaje de registro)
Ejemplo:
/finish 241""",
            not_authorized="""Solo los administradores u organizadores de torneos pueden eliminar usuarios""",
            already_finished=
            """Este torneo ya ha finalizado. Por favor, proporciona un id de torneo correcto
(puedes encontrarlo en el mensaje de registro)
Ejemplo:
/finish 241""",
            congratulation=
            """Campeón supremo: {}
Digno adversario: {}
Al carrer: {}

Felicidades a todos los participantes. ¡Hasta el próximo torneo!""",
            congratulation_for_two=
            """Campeón supremo: {}
Digno adversario: {}
Felicidades a todos los participantes. ¡Hasta el próximo torneo!""",
        ),

        language=dict(
            available_languages=
            """Actualmente puedo hablar en {}. Mi idioma en este chat es: {}. Puedes elegir idioma para este chat de esta manera:
/language en""",
            not_supported="Lo siento, ese idioma todavía no está soportado",
            language_updated="Idioma para este chat actualizado a {}",
        ),

        help=dict(
            help=
            """*Descripción*

Este bot ayuda a organizar torneos:
  - sistema suizo,
  - round robin,
  - eliminación simple,
  - doble eliminación.

*Comandos*

Para iniciar un nuevo torneo, escribe el comando /newtourney@PokeTourneyBot en un grupo.
Si no hay más bots que reaccionen a este comando, es suficiente con /newtourney.

_/newtourney@PokeTourneyBot 22:00 sw Liga Ultra, tipos: Hielo, Normal, Hada, Planta_

22:00 - Inicio del torneo.
sw - tipo de torney (sw, rr, se, de).
Todo lo que se escriba después del tipo se considerará el título del torneo.
Este comand crea un mensaje donde la gente puede apuntarse al torneo.
Puedes omitir el tipo de torneo: el suizo se creará por defecto.

_/starttourney 10251_

Este comando inicia el torneo con id 10251. Lo encontrarás en el mensaje de registro generado al crear el torneo.
El bot publicará un mensaje con un enlace a challonge.com, donde el torneo se crea realmente.

_/result 10251 @neo 2:1 @morpheus_

Este comando registra el resultado del combate entre los jugadores con alias de telegram @neo y @morfeo.
La puntuación se reporta en formato 2:1. El empate se reporta como 0:0.
Si quieres cambiar los resultados de un combate, simplemente modifica el mensaje original o escribe de nuevo el comando con el resultado correcto.
Con esta versión del comando puedes editar combates de rondas previas, lo cual no es posible con el comando corto equivalente (ver "Comandos cortos" más adelante).
Los emparejamientos de la ronda actual pueden cambiar después de editar el partido de la ronda anterior.
En algunos casos, en torneos de doble eliminación, es posible que deba especificar la ronda:

_/result 10251 @neo 2:1 @morpheus w1_
_/result 10251 @trinity 2:1 @morpheus l1_

Las rondas en el grupo de ganadores deben anteponerse con 'w' (por ejemplo, w1, w2), 'l' - para el grupo de perdedores (por ejemplo, l1, l2).

Tras terminar todas las rondas, debes finalizar el torneo. Asegura que todos los resultados en challonge.com son correctos.
Solo es posible modificarlos (mediante los comandos del bot) antes de finalizar el torneo.

_/finish 10251_

Finaliza el torneo. El bot responderá con los resultados. Solo los administradores o el creador del torneo puede finalizarlo.

_/remove 10251 @neo_

Este comando eliminará a un participante del torneo. Obtendrá una derrota técnica en los combates pendientes y no participará en más combates.
Solo los administradores o creadores de un torneo pueden eliminar participantes. ¡Cuidado! Este comando es irreversible, no es posible volver a añadir un participante eliminado.

*Comandos cortos*

Para facilitar el uso de los comandos (especialmente reportando resultados), puedes usar variantes cortas para _/result_, _/finish_, _/remove_.
Estos comandos se aplicarán para el torneo activo (el más recientemente creado con el comando /starttourney).

_/result 2:1_

Reportará una victoria de quien utiliza el comando con un resultado de 2:1.
En algunos casos (en el torneo round robin) deberías especificar una ronda como esta:

_/result 2:1 2_

La ronda viene después de la resultado, separada por espacio.

_/finish_

Finaliza el torneo activo.

_/remove_ @neo

Elimina al usuario @neo del torneo activo.

¡Buena suerte en vuestros torneos!

Si tienes sugerencias, preguntas adicionales o algo no funciona bien, puedes comunicarlo a través del grupo de telegram: https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ
""",
        ),

        common=dict(
            internal_error="Ups, algo falló. Si el problema persiste, dínoslo: https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ",
        )
    ),

    it=dict(
        newtourney=dict(
            hint=
            """Per favore, inserire l’orario di inizio, modalità e descrizione nel seguente modo:
/newtourney hh:mm modalità Descrizione Torneo
Esempio:
/newtourney 22:00 sw Campionato Casuale
Modalità di torneo disponibili:
  sw – svizzero
  rr - Campionato all'italiana
  se – eliminazione singola
  de – doppia eliminazione
""",
            create_in_progress="Creazione…",
            registration_words="Torneo|Inizia alle|Modalità|Creatore|Partecipa|Non partecipa|Forse",
            button_words="Si|No|Forse",
        ),
        starttourney=dict(
            hint=
            """Per favore, inserire l'ID del torneo (si trova nel messaggio di registrazione)
Esempio:
/starttourney 241
""",
            tourney_not_found=
            """Torneo non trovato. Per favore, inserisci un ID corretto (si trova nel messaggio di registrazione)
Esempio:
/starttourney 241
""",
            already_finished=
            """Questo torneo è già terminato. Hai sbagliato ID?
Esempio:
/starttourney 241
""",
            already_started="Questo torneo è già iniziato",
            gather_more_people="Per favore, raggiungi un numero maggiore di giocatori e riprova",
            failed_to_start_tourney="Avvio del torneo non riuscito",
            tourney_started=
            """Il torneo è iniziato. Buona fortuna!
Controlla qui i dettagli: {}
""",
        ),
        result=dict(
            hint_short=
            """Per favore, inserisci il punteggio nella forma corretta. Ad esempio:
/result 2:1
""",
            no_active_tourney="Impossibile trovare il torneo. Ti sei dimenticato di farlo partire?",
            hint_long=
            """Per favore, inserisci il punteggio nella forma corretta.
Forma completa:
/result 241 @neo 2:1 @morpheus
(comando, ID torneo, nick 1, punteggio, nick 2 separati da uno spazio)
Forma breve:
/result 2:1""",
            tourney_not_found="Torneo non trovato. Hai sbagliato ID?",
            user_not_found="Non sei nella lista dei partecipanti",
            match_not_found="La tua partita non è stata trovata. Hai forse abbandonato?",
            tourney_finished="Questo torneo è già terminato. Puoi inviare i risultati solo nei tornei attivi 😉",
            submitted="Il risultato della partita tra {} e {} è stato inviato",
            resubmitted="Il risultato della partita tra {} e {} è stato aggiornato",
            round_needed=
            """Trovate diverse partite per questa coppia. Specificare il round.
Per i round nel girone vincenti bisogna anteporre 'w' al numero del round (e.g. w1, w2), 'l' – per il girone dei perdenti (e.g. l1, l2).
Ad esempio:
/result 10251 @neo 2:1 @morpheus w1
/result 10251 @trinity 2:1 @morpheus l1
            """,
            round_needed_short=
            """Trovate partite in svariati round. Per favore, specifica il round separato da uno spazio, così:
/result 2:1 1
            """,
            round_finished=
            """Round {} finito.
Controlla qui il tuo prossimo avversario: {}""",
            all_rounds_finished="Tutti i round sono finiti. Puoi concludere il torneo. Ma prima assicurati che tutti i risultati siano corretti 😉",
        ),
        remove=dict(
            hint=
            """Per favore, usa il comando in questa forma:
Forma breve:
/remove @neo
Forma completa:
/remove 241 @neo
""",
            tourney_not_found="Non ci sono tornei attivi in questa chat",
            already_finished="Impossibile rimuovere i partecipanti: il torneo è già terminato",
            user_not_found="Impossibile trovare il partecipante in questo torneo",
            not_authorized="Solo gli amministratori e il creatore del torneo possono rimuovere i partecipanti",
            participant_removed=(
                "Il partecipante {} è stato rimosso dal torneo. "
                "Ha avuto problemi tecnici e non prenderà parte alle prossime partite del torneo"
            ),
            round_finished=
            """Round {} terminato.
Controlla qui il tuo prossimo avversario: {}""",
            all_rounds_finished="Tutti i round sono terminati. Puoi concludere il torneo. Ma prima assicurati che tutti i risultati siano corretti 😉",
        ),
        finish=dict(
            hint=
            """Per favore, inserisci l'ID del torneo (si trova nel messaggio di registrazione) o usa una forma abbreviata.
Esempio:
/finish 241""",
            tourney_not_found=
            """Torneo non trovato. Per favore, inserisci l'ID corretto del torneo (si trova nel messaggio di registrazione)
Esempio:
/finish 241""",
            not_authorized="""Solo gli amministratori e il creatore del torneo possono terminare un torneo""",
            already_finished=
            """Il torneo è già terminato. Per favore, se vuoi terminare un vecchio torneo inattivo, inserisci l'ID di quel torneo.
Esempio:
/finish 241""",
            congratulation=
            """Campione supremo: {}
Degno avversario: {}
Magari la prossima volta: {}
Congratulazioni a tutti i partecipanti! Buona fortuna per i prossimi tornei!""",
            congratulation_for_two=
            """Campione supremo: {}
Degno avversario: {}
Congratulazioni a tutti i partecipanyi! Buona fortuna per i prossimi tornei!""",
        ),
        language=dict(
            available_languages=
            """Lingue attualmente disponibili {}. La lingua utilizzata in questa chat: {}. Puoi selezionare la lingua così,Esempio:
/language en""",
            not_supported="Mi dispiace, questa lingua non è ancora supportata",
            language_updated="Lingua cambiata in {}",
        ),
        help=dict(
            help=
            """*Descrizione*

Questo bot aiuta ad organizzare tornei:
  - sistema svizzero,
  - campionato all'italiana,
  - eliminazione singola,
  - doppia eliminazione.
*Comandi*
Per iniziare un nuovo torneo invia il comando /newtourney@PokeTourneyBot in una chat di gruppo.
Se non ci sono altri bot è sufficiente il comando /newtourney, senza bisogno di aggiungere @PokeTourneyBot. Ad esempio:
_/newtourney@PokeTourneyBot 22:00 sw Great League, types: Ice, Normal, Fairy, Grass_
22:00 – orario di inizio del torneo.
sw – tipologia torneo (sw, rr, se, de).
Tutto ciò che verrà aggiunto dopo la tipologia di torneo sarà il titolo dello stesso.
Questo comando creerà un messaggio, col quale i partecipanti potranno registrarsi al torneo.
È possibile non specificare la tipologia di torneo: verrà impostato il sistema svizzero come predefinito.
_/starttourney 10251_
Avvia il torneo con ID 10251. Troverai l'ID nel messaggio di registrazione, dopo aver creato il torneo.
Il bot pubblicherà messaggi con l'url di challonge.com, dove è stato creato il torneo.
_/result 10251 @neo 2:1 @morpheus_
Invia il risultato della partita tra i giocatori con il nick di telegram @neo and @morpheus.
Il punteggio va inviato in formato 2:1. In caso di pareggio va inviato 0:0.
Se si vuole cambiare il risultato di una partita, mandare nuovamente lo stesso messaggio con il risultato corretto.
Con questo tipo di comando puoi modificare anche i risultati delle partite precedenti, che non è possibile fare con il comando in forma breve (guarda "Comandi brevi" sotto).
Le coppie della partita attuale potrebbero cambiare in caso di modifica dei risultati delle partite precedenti.
In alcuni casi nella tipologia doppia eliminazione sarà necessario specificare il round della partita:
_/result 10251 @neo 2:1 @morpheus_ w1
_/result 10251 @trinity 2:1 @morpheus_ l1
Per specificare il round nel girone vincenti bisogna anteporre 'w' al numero del round (e.g. w1, w2), 'l' – per il girone dei perdenti (e.g. l1, l2).
Dopo che tutte le partite saranno terminate, puoi concludere il torneo. Assicurati che tutti i risultati sul sito challonge.com siano corretti.
Puoi sempre modificarli (tramite i comandi del bot) prima della conclusione del torneo.
_/finish 10251_
Conclude un torneo. Il bot risponderà con il risultato finale. Solo gli amministratori e il creatore del bot posso concludere un torneo.
_/remove 10251 @neo_
Rimuove il partecipante dal torneo. Ha avuto problemi tecnici e non parteciperà alle prossime partite del torneo.
Solo gli amministratori e il creatore del torneo possono rimuovere un partecipante. Attenzione! Questo comando non è reversibile. Non si potrà inserire nuovamente il partecipante.
*Comandi brevi*
Per semplificare i comandi (specialmente per inviare i risultati), puoi usare la forma breve per _/result_, _/finish_, _/remove_.
Questi comandi avranno efficacia sul "torneo attivo" (l’ultimo torneo che è stato attivato con il comando /starttourney).
_/result 2:1_
Il vincitore invierà il risultato al bot 2:1.
In alcuni casi (nel torneo round robin) dovrai specificare il round così:
_/result 2:1 2_
Il round va scritto dopo il punteggio, separato da uno spazio.
_/finish_
Conclude il torneo attivo.
_/remove_ @neo
Rimuove i partecipanti con nick @neo dal torneo attivo.
Buona fortuna per i tornei!
Se avete suggerimenti, domande o qualcosa non funziona bene, chiedete nel gruppo telegram: https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ
""",
        ),
        common=dict(
            internal_error="Ops, qualcosa è andato storto. Se dovesse succedere ancora scrivete a https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ",
        )
    ),
    pt=dict(
        newtourney=dict(
            hint=
            """Por favor, indique a hora e descrição no seguinte formato:
/newtourney hh:mm tourney_type Nome do Torneio
Exemplo:
/newtourney 22:00 sw Great League, aleatório 
Tipo de torneios disponíveis :
  sw – Sistema Suíço 
  rr – Todos contra todos 
  se – eliminação simples
  de – eliminação dupla
""",
            create_in_progress="A criar...",
            registration_words="Toneio|Começa às|Tipo|Criador|Participante|Não vai participar|Talvez",
            button_words="Vou participar|Não vou participar|Talvez ",
        ),
        starttourney=dict(
            hint=
            """Por favor indica o ID do torneio (podes encontrá-lo na mensagem de criação do mesmo)
Exemplo:
/starttourney 241
""",
            tourney_not_found=
            """Torneio não encontrado. Por favor indica o ID do torneio (podes encontrá-lo na mensagem de criação do mesmo) 
Example:
/starttourney 241
""",
            already_finished=
            """Torneio indicado terminado, talvez ID incorreto?? 
Example:
/starttourney 241
""",
            already_started="Este torneio já foi iniciado",
            gather_more_people="Por favor, arranja mais participantes e tenta novamente",
            failed_to_start_tourney="Falha ao iniciar o torneio",
            tourney_started=
            """Torneio iniciado. Boa sorte! 
Detalhes do torneio : {}
""",
        ),
        result=dict(
            hint_short=
            """Por favor, indica o resultado de maneira correta, por exemplo:
/result 2:1
""",
            no_active_tourney="Não consegui obter o torneio ativo, pretende iniciar um?",
            hint_long=
            """Por favor, indique o resultado de maneira correcta.
Versão completa:
/result 241 @neo 2:1 @morpheus
(command, tourney id, nick1, score, nick2 separated by spaces)
Versão curta:
/result 2:1""",
            tourney_not_found="Torneio não encontrado, talvez ID incorreto?",
            user_not_found="Não faz parte da lista de participantes",
            match_not_found="Não foi possível encontrar a sua batalha, talvez foi eliminado?",
            tourney_finished="Torneio já terminado, só pode reportar resultados num torneio activo 😉",
            submitted="Resultado entre {} e {} foi submetido",
            resubmitted="Resultado entre {} e {} foi atualizado",
            round_needed=
            """Encontrei algumas batalhas entre este par, por favor especifique a ronda.
As rondas no grupo de vencedores devem ser precedidas  de 'w' (exemplo w1, w2), 'l' – para o grupo dos derrotados (exemplo. l1, l2).
Por exemplo:
/result 10251 @neo 2:1 @morpheus w1
/result 10251 @trinity 2:1 @morpheus l1
            """,
            round_needed_short=
            """Encontrei algumas batalhas em algumas rondas. Por favor , especifica a ronda delimitada por um espaço:
/result 2:1 1
            """,
            round_finished=
            """Ronda {} terminada.
Verifica aqui o teu próximo oponente: {}""",
            all_rounds_finished="Todas as rondas estão terminadas. Pode finalizar o torneio mas primeiro verifique se os resultados estão correctos 😉",
        ),
        remove=dict(
            hint=
            """Por favor , utilize os seguintes formatos para este comando:
Curto:
/remove @neo
Longo:
/remove 241 @neo
""",
            tourney_not_found="Não há torneio ativo neste grupo",
            already_finished="Não é possível retirar esse usuário, o torneio chegou ao fim",
            user_not_found="Não foi possível encontrar esse jogador num torneio ativo",
            not_authorized="Apenas admistradores ou criadores do torneio podem remover jogadores",
            participant_removed=(
                "Participante {} foi removido do torneio ativo. "
                "Foi dada uma derrota técnica, não participará em mais nenhuma batalha"
            ),
            round_finished=
            """Round {} finished.
Verifica o teu próximo adversário : {}""",
            all_rounds_finished="Todas as rondas estão terminadas. Pode finalizar o torneio mas primeiro verifique se os resultados estão correctos 😉",
        ),
        finish=dict(
            hint=
            """Por favor indica o ID do torneio (podes encontrá-lo na mensagem de criação do mesmo)
Exemplo:
/finish 241""",
            tourney_not_found=
            """Torneio não encontrado. Por favor indica o ID do torneio (podes encontrá-lo na mensagem de criação do mesmo)
Example:
/finish 241""",
            not_authorized="""Apenas admistradores ou criador do torneio podem eliminar participantes""",
            already_finished=
            """Este torneio foi concluido. Por favor, indique o id do torneio que pretende finalizar
Example:
/finish 241""",
            congratulation=
            """Campeão Supremo: {}
Adversário Digno: {}
Talvez para a próxima : {}

Parabéns a todos os participantes! Boa sorte para futuros torneios!""",
            congratulation_for_two=
            """Campeão Supremo: {}
Adversário Digno: {}

Parabéns a todos os participantes! Boa sorte para futuros torneios!""",
        ),
        language=dict(
            available_languages=
    """Actualmente posso falar em {}. O meu idioma neste chat é: {}. Pode alterar o idioma do seguinte modo:
/language en""",
            not_supported="Perdão, esse idioma ainda não está disponível",
            language_updated="O idioma para este grupo foi actualizado para {}",
        ),
        help=dict(
            help="""*Descrição*

Este bot ajudará a organizar torneios:
- Sistema Suíço,
- Todos contra todos,
- eliminação simples,
- eliminação dupla.

*Comandos*
Para dar início à um novo torneio escreve o comando /newtourney@PokeTourneyBot para este groupo.
Se não houver mais nenhum bot que reaja a este comando, basta enviar /newtourney. Por exemplo :
_/newtourney@PokeTourneyBot 22:00 sw Great League, Halloween cup_
22:00 – Hora de início do torneio.
sw – tipo de torneio(sw, rr, se, de).
Tudo que for escrito após o tipo de torneio será definido como titulo
O tipo de torneio pode ser dispensado, criado um torneio de sistema suíço.
_/starttourney 10251_
Este comando dá início ao torneio com o id 10251.o id do torneio pode ser encontrado na mensagem de registo do torneio que está após s seguir à de criação do mesmo.
O bot publicará a mensagem para com o link do challonge.com, onde se pode encontrar as informações sobre o torneio

_/result 10251 @neo 2:1 @morpheus_

Este comando regista o resultado da batalha entre os jogadores @neo e @morpheus.
Os resultados devem ser reportados no formato de 2:1. Em caso de empate deverá ser 0:0.
Se pretende mudar o resultado de alguma das batalhas deverá modificar a menagem original com o resultado pretendido. 
Com este comando é possível alterar resultados de rondas anteriores, no qual não seria possível comos método de comando curto(ver "Comandos Curtos" mais à frente).
Os encontros da ronda atual podem ser alterado se houver trocas de resultados nas rondas anteriores.
Em alguns casos, em torneios de eliminação dupla, deverá especificar a ronda:
_/result 10251 @neo 2:1 @morpheus w1_
_/result 10251 @trinity 2:1 @morpheus l1_

As rondas no grupo de vencedores devem ser precedidas  de 'w' (exemplo w1, w2), 'l' – para o grupo dos derrotados (exemplo. l1, l2).

Depois de todas as rondas terminadas deverá finalizar o torneio. Verifique se os resultados estão correctos. 

Apenas estará disponível para alteração de resultados (usando os comandos do bot) em torneios por finalizar.

_/finish 10251_
Termina o torneio. O bot irá responder com os resultados do mesmo. Apenas admistrador e criador do torneio poderão finaliza lo. 

_/remove 10251 @neo_
Retirará o participante do torneio fornecendo uma derrota técnica em todos os futuros encontros do torneio. 
Apenas admistradores e criador do torneio tem permissão para remover participantes, mas atenção esta acção não é reversível, o participante ficará de fora no resto do torneio.

*Comandos Curtos*

Para facilitar no uso dos comandos(especialmente em caso de fornecimento de resultados) poderá utilizar as variantes curtas como_/result_, _/finish_, _/remove_.

Estes comandos estão disponíveis para os torneios ativos (ou no último criado começado com o comando starttourney).

_/result 2:1_

Irá reportar a vitória do jogador que utilizará este comando com o resultado de 2:1.
Em alguns casos, nos torneios de todos contra todos, deverá especificar a ronda em questão como por exemplo:
_/result 2:1 2_

A ronda deverá ser colocada após o resultado, separado por um espaço. 

_/finish_
Terminará o torneio ativo.

_/remove @neo_
Irá remover do torneio o participante com o nome @neo.

Boa sorte nos vossos torneios!!! 

Se tiver alguma sugestão, alguma dúvida ou se encontrar algum erro no bot, dirija se ao nosso grupo de telegram: https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ
""",
        ),
        common=dict(
            internal_error="Uuppss, algo correu mal. Se o problema persistir diga nos em  https://t.me/joinchat/DwJu704gxK-8oDkzdcAhCQ",
        )
    ),
)

import os
import time
from decimal import Decimal

import boto3
from botocore.exceptions import ClientError
from python_dynamodb_lock.python_dynamodb_lock import DynamoDBLockClient


class SettingsTable:
    table_name = os.environ.get('SETTINGS_TABLE_NAME')

    def __init__(self):
        self._dynamodb = boto3.resource('dynamodb')
        self._table = self._dynamodb.Table(self.table_name)

    def write_settings(self, settings):
        print('Writing settings to db: %s', settings.to_dict())
        try:
            item = settings.to_dict()
            self._table.put_item(Item=item)
        except ClientError as e:
            print('Failed to write item: %s' % e.response['Error']['Message'])
            raise

    def get_settings(self, chat_id):
        try:
            print("Querying tourney with id: %s" % chat_id)
            response = self._table.get_item(Key={'chat_id': str(chat_id)})
            print("Response: %s" % response)
        except ClientError as e:
            print('Failed to read item: %s' % e.response['Error']['Message'])
            raise
        else:
            item = response['Item']
            print("GetItem succeeded: %s" % item)
            return item


class TourneyTable:
    table_name = os.environ.get('TOURNEYS_TABLE_NAME')
    lock_table_name = os.environ.get('DYNAMO_LOCK_TABLE_NAME')

    def __init__(self):
        self._dynamodb = boto3.resource('dynamodb')
        self._table = self._dynamodb.Table(self.table_name)
        self.lock_client = DynamoDBLockClient(
            self._dynamodb, table_name=self.lock_table_name)

    def write_tourney(self, tourney):
        print('Writing tourney to db: %s', tourney.to_dict())
        try:
            item = tourney.to_dict()
            self._table.put_item(Item=item)
        except ClientError as e:
            print('Failed to write item: %s' % e.response['Error']['Message'])
            raise

    def get_tourney(self, tourney_id):
        try:
            print("Querying tourney with id: %s" % tourney_id)
            response = self._table.get_item(Key={'id': str(tourney_id)})
            print("Response: %s" % response)
        except ClientError as e:
            print('Failed to read item: %s' % e.response['Error']['Message'])
            raise
        else:
            item = response['Item']
            print("GetItem succeeded: %s" % item)
            return item

    def get_active_tourney(self, chat_id):
        try:
            response = self._table.query(
                IndexName='active_tourney',
                KeyConditionExpression="#I = :chat_id AND #T > :timestamp",
                ExpressionAttributeNames={
                    "#I": "chat_id",
                    "#T": "started_at",
                },
                ExpressionAttributeValues={
                    ":chat_id": str(chat_id),
                    ":timestamp": Decimal(time.time() - 60 * 60 * 24 * 60),
                },
            )
        except ClientError as e:
            print('Failed to read item: %s' % e.response['Error']['Message'])
            raise
        else:
            return response['Items'][-1]

    def write_tourney_with_lock(self, tourney):
        with self.lock_client.acquire_lock(str(tourney.id)):
            self.write_tourney(tourney)


from enum import Enum
from uuid import uuid4

import challonge

from .env import CHALLONGE_APIKEY, CHALLONGE_USERNAME


challonge.set_credentials(CHALLONGE_USERNAME, CHALLONGE_APIKEY)


class UserNotFound(Exception):
    pass


class MatchNotFound(Exception):
    pass


class RoundNeeded(Exception):
    pass


class TourneyTypes(Enum):
    sw = 'swiss'
    rr = 'round robin'
    se = 'single elimination'
    de = 'double elimination'


class ChallongeTourney:
    def __init__(self, id, name, url, tourney_type: TourneyTypes):
        self.id = id
        self.name = name
        self.url = url
        self.tourney_type = tourney_type

    @classmethod
    def create_tourney(cls, name, tourney_type: TourneyTypes):
        url = str(uuid4())[:5]
        name = name[:60]
        tourney_json = challonge.tournaments.create(
            name, url, tournament_type=tourney_type.value, game_name='Pokemon GO',
            show_rounds=True, grand_finals_modifier='single match')
        print(f'tourney: {tourney_json}')
        return ChallongeTourney(
            tourney_json['id'], name,
            tourney_json['full_challonge_url'], tourney_type)

    def bulk_add_participants(self, users):
        challonge.participants.bulk_add(
            self.id, [u.name for u in users])

    def _round_finished(self, matches, round):
        # Challonge marks double elimination loser bracket rounds with minus sign.
        # So, also check for -round.
        current_matches = filter(lambda m: m['round'] in [round, -round], matches)
        not_finished = list(filter(lambda m: m['state'] == 'open', current_matches))
        if len(not_finished) == 0:
            return True
        return False

    def _last_round_finished(self, matches):
        not_finished = list(filter(lambda m: m['state'] in ['open', 'pending'], matches))
        # Workaround for second winner-loser match in double elimination
        if self.tourney_type == TourneyTypes.de and len(not_finished) == 1:
            return True
        if len(not_finished) == 0:
            return True
        return False

    @staticmethod
    def _get_match_by_round(matches, round):
        for m in matches:
            if str(m['round']) == str(round):
                return m

    def report_result_short(self, winner, loser, winner_points,
                            loser_points, tie, current_round, requested_round):
        # Workaround for people without nicknames
        winner = winner.replace('+', ' ') if winner is not None else winner
        loser = loser.replace('+', ' ') if loser is not None else loser

        participants = challonge.participants.index(self.id)
        print(f"participants: {participants}")
        print(f"winner: {winner}, loser: {loser}")
        winner_id, loser_id = None, None
        if winner:
            winner_id = self._get_user_id_by_name(winner, participants)
        elif loser:
            loser_id = self._get_user_id_by_name(loser, participants)
        else:
            raise UserNotFound()
        matches = challonge.matches.index(self.id)
        print(f"matches: {matches}")
        print(f"winner_id: {winner_id}, loser_id: {loser_id}")

        player_matches = self._get_matches_by_participant_id(winner_id or loser_id, matches)
        print(f"player matches: {player_matches}")

        if self.tourney_type == TourneyTypes.rr:
            if not requested_round:
                raise RoundNeeded()
            else:
                match = self._get_match_by_round(player_matches, requested_round)
                if not match:
                    raise MatchNotFound()
        elif len(player_matches) > 0:
            match = player_matches[-1]
        else:
            raise MatchNotFound()

        print(f"state: {match['state']}")

        was_reopened = False
        if match['state'] == 'complete':
            challonge.matches.reopen(self.id, match['id'])
            was_reopened = True

        if winner_id is None:
            if loser_id == match['player1_id']:
                winner_id = match['player2_id']
                score = '-'.join([str(loser_points), str(winner_points)])
            else:
                winner_id = match['player1_id']
                score = '-'.join([str(winner_points), str(loser_points)])
        else:
            if winner_id == match['player1_id']:
                score = '-'.join([str(winner_points), str(loser_points)])
                loser_id = match['player2_id']
            else:
                score = '-'.join([str(loser_points), str(winner_points)])
                loser_id = match['player1_id']

        winner = self._get_user_name_by_id(winner_id, participants)
        loser = self._get_user_name_by_id(loser_id, participants)

        print(f"match: {match}")
        if tie:
            challonge.matches.update(self.id, match['id'], scores_csv=score, winner_id='tie')
        else:
            challonge.matches.update(self.id, match['id'], scores_csv=score, winner_id=winner_id)

        matches = challonge.matches.index(self.id)
        print(f"matches: {matches}")
        return (
            self._round_finished(matches, current_round),
            self._last_round_finished(matches),
            was_reopened,
            winner,
            loser,
        )

    def report_result(self, score, winner, loser, current_round, requested_round, tie):
        # Workaround for people without nicknames
        winner = winner.replace('+', ' ')
        loser = loser.replace('+', ' ')

        was_reopened = False
        participants = challonge.participants.index(self.id)
        print(f"participants: {participants}")
        winner_id = self._get_user_id_by_name(winner, participants)
        print(f"winner id: {winner_id}")
        loser_id = self._get_user_id_by_name(loser, participants)
        print(f"loser id: {loser_id}")

        challonge_matches = challonge.matches.index(self.id)
        print(f"matches: {challonge_matches}")
        matches = self._get_matches_by_participant_ids(winner_id, loser_id, challonge_matches)

        if len(matches) == 1:
            match = matches[0]
        elif len(matches) > 1 and not requested_round:
            raise RoundNeeded()
        else:
            match = self._get_match_by_round(matches, requested_round)
            if not match:
                raise MatchNotFound()

        print(f"state: {match['state']}")
        if match['state'] == 'complete':
            challonge.matches.reopen(self.id, match['id'])
            was_reopened = True

        winner_num = 1 if winner_id == match['player1_id'] else 2
        score = sorted(score, reverse=True)
        score = (
            '-'.join(map(str, [score[0], score[1]]))
            if winner_num == 1
            else '-'.join(map(str, [score[1], score[0]]))
        )
        print(f"match: {match}")
        if tie:
            challonge.matches.update(self.id, match['id'], scores_csv=score, winner_id='tie')
        else:
            challonge.matches.update(self.id, match['id'], scores_csv=score, winner_id=winner_id)

        matches = challonge.matches.index(self.id)
        return (
            self._round_finished(matches, current_round),
            self._last_round_finished(matches),
            was_reopened,
        )

    def start_tourney(self):
        challonge.participants.randomize(self.id)
        return challonge.tournaments.start(self.id)

    def finalize(self):
        return challonge.tournaments.finalize(self.id, include_participants=1)

    def remove_participant(self, participant_nick, round):
        participants = challonge.participants.index(self.id)
        print(f"participants: {participants}")
        participant = self._get_user_by_name(participant_nick, participants)
        if not participant or not participant["active"]:
            raise UserNotFound()
        challonge.participants.destroy(self.id, participant["id"])
        matches = challonge.matches.index(self.id)
        return (
            self._round_finished(matches, round),
            self._last_round_finished(matches),
        )

    @classmethod
    def from_dict(cls, tourney_dict):
        return ChallongeTourney(tourney_dict['id'], tourney_dict['name'], tourney_dict['url'])

    def to_dict(self):
        return {
            'id': self.name,
            'name': self.id,
            'url': self.url,
        }

    @staticmethod
    def _get_user_id_by_name(name, participants):
        for participant in participants:
            if participant['name'].lower() == name.lower():
                return participant['id']
        return None

    @staticmethod
    def _get_user_by_name(name, participants):
        for participant in participants:
            if participant['name'].lower() == name.lower():
                return participant
        return None

    @staticmethod
    def _get_user_name_by_id(id, participants):
        for participant in participants:
            if str(participant['id']) == str(id):
                return participant['name']
        return None

    @staticmethod
    def _get_matches_by_participant_ids(pl1_id, pl2_id, matches):
        result_matches = []
        for match in matches:
            pl1_id_cur = str(match['player1_id'])
            pl2_id_cur = str(match['player2_id'])
            if {str(pl1_id), str(pl2_id)} == {pl1_id_cur, pl2_id_cur}:
                result_matches.append(match)
        return result_matches

    @staticmethod
    def _get_matches_by_participant_id(pl_id, matches):
        player_matches = []
        for match in matches:
            pl1_id_cur = str(match['player1_id'])
            pl2_id_cur = str(match['player2_id'])
            round_cur = str(match['round'])
            print(f"{round_cur}, {pl1_id_cur}, {pl2_id_cur}, {pl_id}")
            if str(pl_id) in {pl1_id_cur, pl2_id_cur} and match['state'] != 'pending':
                player_matches.append(match)
        player_matches.sort(key=lambda x: x['suggested_play_order'])
        return player_matches

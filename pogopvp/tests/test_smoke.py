import asyncio
from typing import List, Dict

from pytest import fixture, mark
from telethon import TelegramClient


api_id = 1271378
api_hash = 'b306f65a4d43a6eb0b7b5796eb200d22'

@fixture
async def clients() -> Dict[int, TelegramClient]:
    clients = {}
    for i in range(5):
        if i == 2:
            continue
        client = TelegramClient(None, api_id, api_hash)
        client.session.set_dc(2, '149.154.167.40', 80)
        await client.start(
            phone=str(9996621234+i), code_callback=lambda: '22222'
        )
        # Issue a high level command to start receiving message
        await client.get_me()
        clients[i] = client

    yield clients

    for client in clients.values():
        await client.disconnect()
        await client.disconnected


@mark.asyncio
async def test_swiss_basic(clients: Dict[int, TelegramClient]):
    async def get_tourney_chat(client):
        dialogs = await client.get_dialogs()
        return next(filter(lambda x: x.name == 'PokeTourneyBotTests', dialogs))

    first_client = clients[0]
    # Create tourney
    tourney_chat = await get_tourney_chat(first_client)
    async with first_client.conversation(tourney_chat) as conv:
        await conv.send_message('/newtourney 22:00 test tourney')
        await asyncio.sleep(5)
        registration_message = await conv.get_response()
        assert 'test tourney' in registration_message.text

        tourney_id = registration_message.text.split()[1].split(':')[0]
        assert isinstance(int(tourney_id), int)

        # Register in tourney
        for client in clients.values():
            chat = await get_tourney_chat(client)
            await chat.message.click()

        registration_message = (await first_client.get_messages(tourney_chat))[0]
        for client in clients.values():
            user = await client.get_me()
            full_name = ' '.join([user.first_name, user.last_name])
            assert full_name in registration_message.text

        # Start tourney
        await conv.send_message(f'/starttourney {tourney_id}')
        response = await conv.get_response()
        assert 'Your tourney has started' in response.text

        for round_num in range(2):
            # Report results
            for client in clients.values():
                chat = await get_tourney_chat(client)
                await chat.send_message('/result 2:1')
                await asyncio.sleep(3)
                response = await conv.get_response()

        await asyncio.sleep(3)
        # Finish tourney
        await conv.send_message('/finish')
        response = await conv.get_response()
        assert 'Congratulations to all participants' in response.text
